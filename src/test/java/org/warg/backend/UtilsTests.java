package org.warg.backend;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.warg.backend.domain.Core;

@QuarkusTest
public class UtilsTests {

    @Inject Core core;

    @Test
    public void testHelloEndpoint() {
        given()
                .when().get("/utils/hello")
                .then()
                .statusCode(200)
                .body(is("Hello Test"));
    }
    
}
