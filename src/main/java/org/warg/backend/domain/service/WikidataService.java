package org.warg.backend.domain.service;

import javax.json.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import org.warg.backend.domain.Readers.*;

public class WikidataService {

    private static String serverUrl = "https://test.wikidata.org/w/rest.php/wikibase/v0";

    // test item: Q42
    public static JsonObjectSafe get_item(String item_id) {
        try {
            // Step 1: Create a URL object
            String url_string = serverUrl + "/entities/items/" + item_id;
            URL url = new URL(url_string);

            // Step 2: Open a connection to the URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Step 3: Cast the connection to an HttpURLConnection object
            connection = (HttpURLConnection) url.openConnection();

            // Step 4: Set the request method to GET
            connection.setRequestMethod("GET");

            // Step 5: Add any necessary headers to the request
            connection.setRequestProperty("Content-Type", "application/json");

            // Step 6: Send the request
            connection.connect();

            // Step 7: Check the response code
            int responseCode = connection.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            // Step 8: Read the response body
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Print the response body
            System.out.println(response.toString());

            // Step 9: Convert the response body to a JSON object
            JsonReader jsonReader = Json.createReader(new StringReader(response.toString()));
            JsonObject jsonObject = jsonReader.readObject();
            jsonReader.close();

            return new JsonObjectSafe(jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void modify_statement_item(String item_id, String statement_id, String property_id, JsonObjectSafe body) {
        try {
            // Step 1: Create a URL object
            String url_string = serverUrl + "/entities/items/" + item_id + "/statements/" + statement_id;
            URL url = new URL(url_string);

            // Step 2: Open a connection to the URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Step 3: Cast the connection to an HttpURLConnection object
            connection = (HttpURLConnection) url.openConnection();

            // Step 4: Set the request method to PUT
            connection.setRequestMethod("PUT");

            // Step 5: Set the doOutput flag to true and write data to the output stream
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body.toString());
            writer.flush();
            writer.close();

            // Step 6: Add any necessary headers to the request
            connection.setRequestProperty("Content-Type", "application/json");

            // Step 7: Send the request
            connection.connect();

            // Step 8: Check the response code
            int responseCode = connection.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            // Step 9: Read the response body
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Print the response body
            System.out.println(response.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void modify_statement(String item_id, String statement_id, String property_id, JsonObjectSafe body) {
        try {
            // Step 1: Create a URL object
            String url_string = serverUrl + "/statements/" + statement_id;
            URL url = new URL(url_string);

            // Step 2: Open a connection to the URL
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Step 4: Set the request method to PUT
            connection.setRequestMethod("PUT");

            // Step 5: Set the doOutput flag to true and write data to the output stream
            connection.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write(body.toString());
            writer.flush();
            writer.close();

            // Step 6: Add any necessary headers to the request
            connection.setRequestProperty("Content-Type", "application/json");

            // Step 7: Send the request
            connection.connect();

            // Step 8: Check the response code
            int responseCode = connection.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            // Step 9: Read the response body
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Print the response body
            System.out.println(response.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
