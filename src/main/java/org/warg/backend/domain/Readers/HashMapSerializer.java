package org.warg.backend.domain.Readers;
import org.warg.backend.data.model.TemplateHashmap;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Provider
@Produces(MediaType.APPLICATION_JSON)
public class HashMapSerializer implements MessageBodyWriter<TemplateHashmap> {
    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == TemplateHashmap.class;
    }

    @Override
    public long getSize(TemplateHashmap templateHashmap, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(TemplateHashmap templateHashmap, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException {
        // Merge the four HashMaps into one
        Map<String, Object> mergedMap = new HashMap<>();
        mergedMap.putAll(templateHashmap.stringmap);
        mergedMap.putAll(templateHashmap.floatmap);
        mergedMap.putAll(templateHashmap.timestampmap);

        // Serialize the merged HashMap
        ObjectMapper objectMapper = new ObjectMapper();
        String serialized = objectMapper.writeValueAsString(mergedMap);

        // Write the serialized JSON to the HTTP response
        entityStream.write(serialized.getBytes());
    }
}
