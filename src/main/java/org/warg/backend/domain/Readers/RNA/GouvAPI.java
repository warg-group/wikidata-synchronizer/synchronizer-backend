package org.warg.backend.domain.Readers.RNA;

import org.warg.backend.data.model.EntitySearchModel;
import org.warg.backend.data.model.EntityStorable;
import org.warg.backend.domain.Readers.JsonObjectSafe;
import org.warg.backend.domain.Readers.Reader;
import org.warg.backend.data.model.EntityModel;

import javax.json.JsonArray;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class GouvAPI implements Reader {


    public static String getAPIString(String url) {
        String targetURL = "https://entreprise.data.gouv.fr/api/rna/v1/" + url;
        try {
            URL obj = new URL(targetURL);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static EntityModel getNonprofitById(String id) {
        String json = getAPIString("id/" + id);
        if (json == null) {
            return null;
        }
        JsonObjectSafe safe = new JsonObjectSafe(json);
        safe = safe.getJsonObjectSafe("association");

        return new EntityModel(safe);
    }

    public static EntitySearchModel queryPage(EntitySearchModel model, Integer page) throws IOException {
        if (model.crawled_index >= page) {
            return model;
        }
        else if (page == 0 || (model.total_pages != null && model.total_pages < page)) {
            throw new IOException("Invalid page number");
        }
        String json = getAPIString("full_text/" + model.keyword + "?page=" + page.toString());
        if (json == null) {
            throw new IOException("Unsuccessful API call");
        }
        JsonObjectSafe safe = new JsonObjectSafe(json);
        JsonArray list = safe.getJsonArray("association");
        model.per_page = safe.getJsonNumber("per_page").intValue();
        model.total_pages = safe.getJsonNumber("total_pages").intValue();
        model.total_results = safe.getJsonNumber("total_results").intValue();
        System.out.println("per_page" + model.per_page);
        System.out.println("total_pages" + model.total_pages);
        System.out.println("total_results" + model.total_results);
        model.crawled_index++;
        for (int i = 0; i < list.size(); i++) {
            model.entities.add(
                    new EntityStorable(new JsonObjectSafe(list.getJsonObject(i)), model));
        }
        return model;
    }
}
