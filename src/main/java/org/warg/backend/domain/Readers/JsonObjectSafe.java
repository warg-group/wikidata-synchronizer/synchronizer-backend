package org.warg.backend.domain.Readers;

import javax.json.*;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class JsonObjectSafe {
    public final JsonObject jsonObject;

    public JsonObjectSafe(String string) {
        JsonReader jsonReader = Json.createReader(new StringReader(string));
        this.jsonObject = jsonReader.readObject();
        jsonReader.close();
    }

    public JsonObjectSafe(JsonObject object) {
        this.jsonObject = object;
    }

    public JsonArray getJsonArray(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return jsonObject.getJsonArray(s);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public JsonObject getJsonObject(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return jsonObject.getJsonObject(s);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public JsonObjectSafe getJsonObjectSafe(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return new JsonObjectSafe(jsonObject.getJsonObject(s));
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public JsonNumber getJsonNumber(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return jsonObject.getJsonNumber(s);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public JsonString getJsonString(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return jsonObject.getJsonString(s);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public String getString(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return jsonObject.getString(s);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public Integer getInt(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return (int) jsonObject.getInt(s);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public Boolean getBoolean(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return jsonObject.getBoolean(s);
        } catch (ClassCastException ex) {
            return null;
        }
    }

    public LocalDate getLocalDate(String s) {
        if (isNull(s)) {
            return null;
        }
        try {
            return LocalDate.parse(jsonObject.getString(s));
        } catch (DateTimeParseException ex) {
            return null;
        }
    }

    public Boolean isNull(String s) {
        return jsonObject.isNull(s);
    }

    public String toString() {
        return jsonObject.toString();
    }
}
