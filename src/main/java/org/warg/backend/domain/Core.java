package org.warg.backend.domain;

import org.warg.backend.data.model.*;
import org.warg.backend.data.repository.EntityRepository;
import org.warg.backend.data.repository.CollectionRepository;
import org.warg.backend.data.repository.EntitySearchRepository;
import org.warg.backend.data.repository.EntityStorableRepository;
import org.warg.backend.domain.Readers.RNA.GouvAPI;
import org.warg.backend.presentation.model.EntityShortModel;
import org.warg.backend.presentation.model.CollectionPresentationModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@ApplicationScoped
public class Core {

    @Inject
    EntityRepository entityRepository;

    @Inject
    CollectionRepository collectionRepository;

    @Inject
    EntitySearchRepository entitySearchRepository;

    @Inject
    EntityStorableRepository entityStorableRepository;

    /*
     ** EntitiesEnpoints functions
     */


    /**
     * @brief Refresh the entity with the given id
     *
     * @param id The id of the entity to refresh
     *
     * @return The refreshed entity
     */
    public EntityModel refreshEntity(Long id) {
        return renameEntity(id, entityRepository.getEntities(id).external_id);
    }

    /**
     * @brief Delete the entity with the given id
     *
     * @param id The id of the entity to delete
     *
     * @return True if the entity has been deleted, false otherwise
     */
    public Boolean deleteEntity(Long id) {
        return entityRepository.deleteEntities(id);
    }

    /**
     * @brief Rename the entity with the given id
     *
     * @param id The id of the entity to rename
     * @param name The new name of the entity
     *
     * @return The renamed entity
     */
    public EntityModel renameEntity(Long id, String name) {
        EntityModel assos = entityRepository.getEntities(id);
        EntityModel tmp = GouvAPI.getNonprofitById(name);
        tmp.id = assos.id;
        id = entityRepository.updatedb(tmp);
        return entityRepository.getEntities(id);
    }

    /**
     * @brief Get the list of all entities
     *
     * @return The list of all entities
     */
    public List<EntityModel> Entities() {
        return entityRepository.getEntities();
    }

    /**
     * @brief Get the entity with the given id
     *
     * @param id The id of the entity to get
     *
     * @return The entity with the given id
     */
    public EntityModel getEntity(Long id) {
        return entityRepository.getEntities(id);
    }

    /**
     * @brief Get the entity with the given name
     *
     * @param rnaId The id of the entity to get
     *
     * @return The entity with the given name
     */
    public EntityModel createEntity(String rnaId) {
        EntityModel assos = GouvAPI.getNonprofitById(rnaId);
        Long id = entityRepository.persistdb(assos);
        return entityRepository.getEntities(id);
    }

    /*
     ** CollectionsEnpoints functions
     */


    /**
     * @brief Get the list of all collections
     *
     * @return The list of all collections
     */
    public List<CollectionModel> getCollections() {
        return collectionRepository.getList();
    }

    /**
     * @brief Get the list of all collections and return their names
     *
     * @return The list of all collections names
     */
    public List<String> getCollectionsNames() {
        return collectionRepository.getList().stream().map(s -> s.name).collect(Collectors.toList());
    }

    /**
     * @brief Get the collection with the given id
     *
     * @param id The id of the collection to get
     *
     * @return The collection with the given id
     */
    public CollectionModel getCollection(Long id) {
        return collectionRepository.getCollection(id);
    }

    /**
     * @brief Create a collection with the given name
     *
     * @param name The name of the collection to create
     *
     * @return The created collection
     */
    public CollectionModel createCollection(String name) {
        CollectionModel collection = new CollectionModel(name);
        Long id = collectionRepository.persistdb(collection);
        return collectionRepository.getCollection(id);
    }

    /**
     * @brief Rename the collection with the given id
     *
     * @param id The id of the collection to rename
     * @param name The new name of the collection
     *
     * @return The renamed collection
     */
    public CollectionModel renameCollection(Long id, String name) {
        CollectionModel collection = collectionRepository.getCollection(id);
        collection.name = name;
        id = collectionRepository.updatedb(collection);
        return collectionRepository.getCollection(id);
    }

    /**
     * @brief Delete the collection with the given id
     *
     * @param id The id of the collection to delete
     *
     * @return True if the collection has been deleted, false otherwise
     */
    public Boolean deleteCollection(Long id) {
        return collectionRepository.deleteCollection(id);
    }

    /**
     * @brief Add the entity with the given id to the collection with the given id
     *
     * @param id The id of the collection to add the entity to
     * @param idE The rnaid of the entity to add to the collection
     *
     * @return The collection with the added entity
     */
    public CollectionModel addToCollection(Long id, Long idE) {
        CollectionModel collection = collectionRepository.getCollection(id);
        EntityModel entity = entityRepository.getEntities(idE);
        collection.addToCollectionModel(entity);
        return collectionRepository.getCollection(collectionRepository.updatedb(collection));
    }

    /**
     * @brief Add the entity with the given name to the collection with the given id
     *
     * @param id The id of the collection to add the entity to
     * @param name The name of the entity to add to the collection
     *
     * @return The collection with the added entity
     */
    public CollectionModel createAddToCollection(Long id, String name) {
        Long idE = createEntity(name).id;
        return addToCollection(id, idE);
    }

    /**
     * @brief Remove the entity with the given id from the collection with the given id
     *
     * @param id The id of the collection to remove the entity from
     * @param idE The rnaid of the entity to remove from the collection
     *
     * @return The collection with the removed entity
     */
    public CollectionModel removeFromCollection(Long id, Long idE) {
        CollectionModel collection = collectionRepository.getCollection(id);
        EntityModel entityModel = entityRepository.getEntities(idE);
        collection.removeFromCollectionModel(entityModel);
        entityModel.collectionModels.remove(collection);
        entityRepository.updatedb(entityModel);
        return collectionRepository.getCollection(collectionRepository.updatedb(collection));
    }

    /**
     * @brief Remove the entity with the given name from the collection with the given id
     *
     * @param id The id of the collection to remove the entity from
     *
     * @return The collection with the removed entity
     */

    private List<EntityShortModel> getCollectionEntities(Long id) {
        return getCollection(id).entitiesList.stream().map(EntityShortModel::new).collect(Collectors.toList());
    }

    public CollectionPresentationModel getCollectionPresentation(Long id) {
        return new CollectionPresentationModel(getCollection(id), getCollectionEntities(id));
    }

    public List<EntityStorable> getSearch(Long id, Integer page) {
        EntitySearchModel search = entitySearchRepository.getSearch(id);
        try {
            search = GouvAPI.queryPage(search, page);
            entitySearchRepository.updatedb(search);
            return search.getPage(page);
        } catch (IOException e) {
            System.out.println("Error while crawling");
            e.printStackTrace();
        }
        return null;
    }

    public EntitySearchModel createSearch(String keyword) {
        EntitySearchModel search = new EntitySearchModel(keyword);
        System.out.println("Before persist");
        System.out.println("createSearch: " + search.id);
        Long id = entitySearchRepository.persistdb(search);
        System.out.println("createSearch2: " + search.id);
        System.out.println("After persist");
        try {
            System.out.println("createSearch3: " + search.id);
            search = GouvAPI.queryPage(search, 1);
            System.out.println("createSearch4: " + search.id);
            System.out.println("Before update " + search.entities.size());
            entitySearchRepository.updatedb(search);
            System.out.println("createSearch5: " + search.id);
            System.out.println("After update " + entitySearchRepository.getSearch(id).entities.size());
            return entitySearchRepository.getSearch(id);
        } catch (Exception e) {
            System.out.println("Error while crawling");
            e.printStackTrace();
        }
        return null;
    }

    public List<Long> validateSearch(Long id, Long idc, List<Long> ids) {
        EntitySearchModel search = entitySearchRepository.getSearch(id);
        List<EntityModel> models = search.getEntities(ids);
        System.out.println("validateSearch " + models.size());
        for (EntityModel model : models) {
            model.id = null;
            entityRepository.persistdb(model);
            addToCollection(idc, model.id);
            System.out.println("added " + model.id);
        }
        entitySearchRepository.deleteSearch(id);
        return ids;
    }

    public EntitySearchModel getSearchModel(Long id) {
        return entitySearchRepository.getSearchModel(id);
    }
}
