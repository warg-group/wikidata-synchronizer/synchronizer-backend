package org.warg.backend.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "search")
@JsonInclude(JsonInclude.Include.ALWAYS)
public class EntitySearchModel extends PanacheEntityBase {

    @Id
    @GeneratedValue
    @Column(name = "search_id")
    public Long id;

    @Column(name = "keyword")
    public String keyword;

    @Column(name = "per_page")
    public Integer per_page;

    @Column(name = "total_pages")
    public Integer total_pages;

    @Column(name = "total_results")
    public Integer total_results;

    @Column(name = "crawled_index")
    public Integer crawled_index = 0;

    @OneToMany(mappedBy = "searchModel",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    public List<EntityStorable> entities = new ArrayList<>();

    @JsonIgnore
    public List<EntityStorable> getPage(Integer page) {
        System.out.println("crawled_index: " + crawled_index);
        System.out.println("per_page: " + per_page);
        System.out.println("per_page * page: " + per_page * page);
        System.out.println("(per_page + 1) * page: " + per_page * (page + 1));
        return entities.subList(per_page * (page - 1),
                Math.min(entities.size(), per_page * page));
    }

    @JsonIgnore
    public List<EntityModel> getEntities(List<Long> ids) {
        System.out.println(ids.contains(entities.get(0).id));
        System.out.println(entities.get(0).id);
        System.out.println(ids);
        System.out.println(entities.stream().filter(e -> ids.contains(e.id))
                .collect(Collectors.toList()));
        return entities.stream().filter(e -> ids.contains(e.id))
                .map(EntityModel::new).collect(Collectors.toList());
    }

    public EntitySearchModel(String keyword) {
        this.keyword = keyword;
    }

    public EntitySearchModel() {
        this.keyword = keyword;
    }
}
