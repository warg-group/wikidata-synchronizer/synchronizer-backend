package org.warg.backend.data.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.warg.backend.domain.Readers.JsonObjectSafe;

import javax.persistence.*;

@Entity
@JsonInclude(JsonInclude.Include.ALWAYS)
public class EntityStorable extends EntityBaseModel {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "search_id" )
    private EntitySearchModel searchModel;

    public EntityStorable() {}

    public EntityStorable(EntityBaseModel entityModel) {
        this.id = entityModel.id;
        this.external_id = entityModel.external_id;
        this.numero_reconnaissance_utilite_publique = entityModel.numero_reconnaissance_utilite_publique;
        this.code_gestion = entityModel.code_gestion;
        this.date_creation = entityModel.date_creation;
        this.date_declaration_dissolution = entityModel.date_declaration_dissolution;
        this.nature = entityModel.nature;
        this.groupement = entityModel.groupement;
        this.titre = entityModel.titre;
        this.titre_court = entityModel.titre_court;
        this.addresse_code_postal = entityModel.addresse_code_postal;
        this.telephone = entityModel.telephone;
        this.site_web = entityModel.site_web;
        this.email = entityModel.email;
    }

    public EntityStorable(JsonObjectSafe json, EntitySearchModel searchModel) {
        super(json);
        this.searchModel = searchModel;
    }
}
