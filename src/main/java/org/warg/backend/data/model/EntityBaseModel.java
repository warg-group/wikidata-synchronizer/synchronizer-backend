package org.warg.backend.data.model;

import java.time.LocalDate;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.warg.backend.domain.Readers.JsonObjectSafe;

@Entity
@Table(name = "entity")
@JsonInclude(JsonInclude.Include.ALWAYS)
public class EntityBaseModel extends PanacheEntityBase {

    @Id
    @GeneratedValue
    @Column(name = "entitybase_id")
    public Long id;

    @Column(name = "external_id")
    public String external_id;

    @Column(name = "numero_reconnaissance_utilite_publique")
    public Integer numero_reconnaissance_utilite_publique;

    @Column(name = "code_gestion")
    public Integer code_gestion;

    @Column(name = "date_creation")
    public LocalDate date_creation;

    @Column(name = "date_declaration_dissolution")
    public LocalDate date_declaration_dissolution;

    @Column(name = "nature")
    public String nature;

    @Column(name = "groupement")
    public String groupement;

    @Column(name = "titre")
    public String titre;

    @Column(name = "titre_court")
    public String titre_court;

    @Column(name = "addresse_code_postal")
    public String addresse_code_postal;

    @Column(name = "telephone")
    public String telephone;

    @Column(name = "site_web")
    public String site_web;

    @Column(name = "email")
    public String email;

    public EntityBaseModel() {}

    public EntityBaseModel(JsonObjectSafe json) {
        this.external_id = json.getString("id_association");
        this.numero_reconnaissance_utilite_publique = json.getInt("numero_reconnaissance_utilite_publique");
        this.code_gestion = json.getInt("code_gestion");
        this.date_creation = json.getLocalDate("date_creation");
        this.date_declaration_dissolution = json.getLocalDate("date_declaration_dissolution");
        this.nature = json.getString("nature");
        this.groupement = json.getString("groupement");
        this.titre = json.getString("titre");
        this.titre_court = json.getString("titre_court");
        this.addresse_code_postal = json.getString("adresse_code_postal");
        this.telephone = json.getString("telephone");
        this.site_web = json.getString("site_web");
        this.email = json.getString("email");
    }

    public EntityBaseModel(EntityStorable entityStorable) {
        this.id = entityStorable.id;
        this.external_id = entityStorable.external_id;
        this.numero_reconnaissance_utilite_publique = entityStorable.numero_reconnaissance_utilite_publique;
        this.code_gestion = entityStorable.code_gestion;
        this.date_creation = entityStorable.date_creation;
        this.date_declaration_dissolution = entityStorable.date_declaration_dissolution;
        this.nature = entityStorable.nature;
        this.groupement = entityStorable.groupement;
        this.titre = entityStorable.titre;
        this.titre_court = entityStorable.titre_court;
        this.addresse_code_postal = entityStorable.addresse_code_postal;
        this.telephone = entityStorable.telephone;
        this.site_web = entityStorable.site_web;
        this.email = entityStorable.email;
    }

    public String toString() {
        return "EntityModel [id=" + id + ", titre=" + titre + "]";
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        EntityBaseModel other = (EntityBaseModel) o;
        // field comparison
        return EntityBaseModel.this.id.equals(other.id);
    }
}
