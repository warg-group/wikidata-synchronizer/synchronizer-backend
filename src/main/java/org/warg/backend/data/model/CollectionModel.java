package org.warg.backend.data.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "collection")
@JsonInclude(JsonInclude.Include.ALWAYS)
public class CollectionModel extends PanacheEntityBase {

    @Id
    @GeneratedValue
    @Column(name = "collection_id")
    public Long id;

    @Column(name = "name")
    public String name;

    @ManyToMany(mappedBy = "collectionModels",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    public List<EntityModel> entitiesList = new ArrayList<>();

    public boolean addToCollectionModel(EntityModel entityId) {
        boolean res = entityId.collectionModels.add(this);
        res &= this.entitiesList.add(entityId);
        return res;
    }

    public boolean removeFromCollectionModel(EntityModel entityId) {
        return this.entitiesList.remove(entityId);
    }

    public CollectionModel() {
        this.entitiesList = new ArrayList<>();
    }

    public CollectionModel(String name) {
        this.name = name;
        this.entitiesList = new ArrayList<>();
    }

    public CollectionModel(CollectionModel collectionModel) {
        this.id = collectionModel.id;
        this.name = collectionModel.name;
        this.entitiesList = collectionModel.entitiesList;
    }

    public String toString() {
        return "CollectionModel [id=" + id + ", name=" + name + ", nb=" + entitiesList.size() + "]";
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;
        // type check and cast
        if (getClass() != o.getClass())
            return false;
        CollectionModel other = (CollectionModel) o;
        // field comparison
        return CollectionModel.this.id.equals(other.id);
    }
}
