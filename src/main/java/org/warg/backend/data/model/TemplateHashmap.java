package org.warg.backend.data.model;

import java.time.LocalDate;
import java.util.HashMap;

public class TemplateHashmap {
    public HashMap<String, String> stringmap;
    public HashMap<String, Float> floatmap;
    public HashMap<String, LocalDate> timestampmap;

    public TemplateHashmap(String str, Float flt, LocalDate date) {
        stringmap = new HashMap<>();
        floatmap = new HashMap<>();
        timestampmap = new HashMap<>();
        stringmap.put("string", str);
        floatmap.put("float", flt);
        timestampmap.put("timestamp", date);
    }

    public TemplateHashmap() {
        stringmap = new HashMap<>();
        floatmap = new HashMap<>();
        timestampmap = new HashMap<>();
    }

    public TemplateHashmap(HashMap<String, String> stringmap, HashMap<String, Float> floatmap, HashMap<String, LocalDate> timestampmap) {
        this.stringmap = stringmap;
        this.floatmap = floatmap;
        this.timestampmap = timestampmap;
    }
}
