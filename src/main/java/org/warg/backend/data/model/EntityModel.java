package org.warg.backend.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.warg.backend.data.repository.EntityRepository;
import org.warg.backend.domain.Readers.JsonObjectSafe;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonInclude(JsonInclude.Include.ALWAYS)
public class EntityModel extends EntityBaseModel {

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            joinColumns = { @JoinColumn(name = "entitybase_id") },
            inverseJoinColumns = { @JoinColumn(name = "collection_id") }
    )
    @JsonIgnore
    public List<CollectionModel> collectionModels;

    public EntityModel() {}

    public EntityModel(JsonObjectSafe json) {
        this.external_id = json.getString("id_association");
        this.numero_reconnaissance_utilite_publique = json.getInt("numero_reconnaissance_utilite_publique");
        this.code_gestion = json.getInt("code_gestion");
        this.date_creation = json.getLocalDate("date_creation");
        this.date_declaration_dissolution = json.getLocalDate("date_declaration_dissolution");
        this.nature = json.getString("nature");
        this.groupement = json.getString("groupement");
        this.titre = json.getString("titre");
        this.titre_court = json.getString("titre_court");
        this.addresse_code_postal = json.getString("adresse_code_postal");
        this.telephone = json.getString("telephone");
        this.site_web = json.getString("site_web");
        this.email = json.getString("email");
    }

    public EntityModel(EntityModel entityModel) {
        this.id = entityModel.id;
        this.external_id = entityModel.external_id;
        this.numero_reconnaissance_utilite_publique = entityModel.numero_reconnaissance_utilite_publique;
        this.code_gestion = entityModel.code_gestion;
        this.date_creation = entityModel.date_creation;
        this.date_declaration_dissolution = entityModel.date_declaration_dissolution;
        this.nature = entityModel.nature;
        this.groupement = entityModel.groupement;
        this.titre = entityModel.titre;
        this.titre_court = entityModel.titre_court;
        this.addresse_code_postal = entityModel.addresse_code_postal;
        this.telephone = entityModel.telephone;
        this.site_web = entityModel.site_web;
        this.email = entityModel.email;
    }

    public EntityModel(EntityStorable entityStorable) {
        this.id = entityStorable.id;
        this.external_id = entityStorable.external_id;
        this.numero_reconnaissance_utilite_publique = entityStorable.numero_reconnaissance_utilite_publique;
        this.code_gestion = entityStorable.code_gestion;
        this.date_creation = entityStorable.date_creation;
        this.date_declaration_dissolution = entityStorable.date_declaration_dissolution;
        this.nature = entityStorable.nature;
        this.groupement = entityStorable.groupement;
        this.titre = entityStorable.titre;
        this.titre_court = entityStorable.titre_court;
        this.addresse_code_postal = entityStorable.addresse_code_postal;
        this.telephone = entityStorable.telephone;
        this.site_web = entityStorable.site_web;
        this.email = entityStorable.email;
    }
}
