package org.warg.backend.data.repository;

import org.warg.backend.data.model.EntitySearchModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class EntitySearchRepository implements DatabaseHandler<EntitySearchModel> {

    @Inject
    EntityManager em;

    @Transactional
    public Long persistdb(EntitySearchModel entitySearchModel){
        em.persist(entitySearchModel);
        em.flush();
        return entitySearchModel.id;
    }

    @Transactional
    public Long updatedb(EntitySearchModel entitySearchModel){
        System.out.println("updatedb: " + entitySearchModel.id);
        em.merge(entitySearchModel);
        System.out.println("merged");
        em.flush();
        return entitySearchModel.id;
    }

    @Transactional
    public List<EntitySearchModel> getSearches() {
        em.flush();
        return EntitySearchModel.findAll().list();
    }

    @Transactional
    public EntitySearchModel getSearch(Long id) {
        em.flush();
        return EntitySearchModel.findById(id);
    }

    @Transactional
    public EntitySearchModel getSearchByKeyword(String keyword) {
        em.flush();
        List<EntitySearchModel> list = EntitySearchModel.list("keyword", keyword);
        return list.get(0);
    }

    @Transactional
    public Boolean deleteEntities(Long id) {
        EntitySearchModel searchModel = getSearch(id);
        em.remove(searchModel);
        return true;
    }

    @Transactional
    public void deleteSearch(Long id) {
        EntitySearchModel.deleteById(id);
    }

    @Transactional
    public EntitySearchModel getSearchModel(Long id) {
        return getSearch(id);
    }
}
