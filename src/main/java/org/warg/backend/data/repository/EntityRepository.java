package org.warg.backend.data.repository;

import org.warg.backend.data.model.EntityModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class EntityRepository implements DatabaseHandler<EntityModel> {

    @Inject
    EntityManager em;

    @Transactional
    public Long persistdb(EntityModel rnadb){
       em.persist(rnadb);
       em.flush();
       return rnadb.id;
    }

    @Transactional
    public Long updatedb(EntityModel rnadb){
        em.merge(rnadb);
        em.flush();
        return rnadb.id;
    }

    @Transactional
    public List<EntityModel> getEntities() {
        em.flush();
        return EntityModel.findAll().list();
    }

    @Transactional
    public EntityModel getEntities(Long id) {
        em.flush();
        return EntityModel.findById(id);
    }

    @Transactional
    public Boolean deleteEntities(Long id) {
        EntityModel assos = getEntities(id);
        em.remove(assos);
        return true;
    }
}
