package org.warg.backend.data.repository;

import org.warg.backend.data.model.EntitySearchModel;
import org.warg.backend.data.model.EntityStorable;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@ApplicationScoped
public class EntityStorableRepository  implements DatabaseHandler<EntityStorable> {

    @Inject
    EntityManager em;
    @Override
    public Long persistdb(EntityStorable entityStorable) {
        em.persist(entityStorable);
        em.flush();
        return entityStorable.id;
    }

    @Override
    public Long updatedb(EntityStorable entityStorable) {
        em.merge(entityStorable);
        em.flush();
        return entityStorable.id;
    }
}
