package org.warg.backend.data.repository;

import org.warg.backend.data.model.CollectionModel;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class CollectionRepository implements DatabaseHandler<CollectionModel> {

    @Inject
    EntityManager em;

    @Transactional
    public Long persistdb(CollectionModel collection){
        em.persist(collection);
        em.flush();
        return collection.id;
    }

    @Transactional
    public Long updatedb(CollectionModel collection){
        em.merge(collection);
        em.flush();
        return collection.id;
    }

    @Transactional
    public List<CollectionModel> getList() {
        em.flush();
        return CollectionModel.findAll().list();
    }

    @Transactional
    public CollectionModel getCollection(Long id) {
        em.flush();
        return CollectionModel.findById(id);
    }

    @Transactional
    public Boolean deleteCollection(Long id) {
        CollectionModel assos = getCollection(id);
        em.remove(assos);
        return true;
    }
}
