package org.warg.backend.data.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;


public interface DatabaseHandler<T> extends PanacheRepository<T> {

    public Long persistdb(T t);

    public Long updatedb(T t);

}
