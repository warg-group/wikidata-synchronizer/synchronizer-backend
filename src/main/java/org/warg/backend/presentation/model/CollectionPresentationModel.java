package org.warg.backend.presentation.model;

import org.warg.backend.data.model.CollectionModel;

import java.util.ArrayList;
import java.util.List;

public class CollectionPresentationModel extends CollectionModel {

    public List<EntityShortModel> entities;

    public CollectionPresentationModel(CollectionModel collectionModel, List<EntityShortModel> entities) {
        super(collectionModel);
        this.entitiesList = null;
        if (entities == null)
            throw new IllegalArgumentException("Entities list cannot be null");
        System.out.println("CollectionPresentationModel: " + entities.size());
        this.entities = new ArrayList<>();
        this.entities.addAll(entities);
    }
}
