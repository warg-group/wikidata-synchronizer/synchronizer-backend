package org.warg.backend.presentation.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.warg.backend.data.model.EntityBaseModel;

@JsonInclude(JsonInclude.Include.ALWAYS)
public class EntityShortModel extends PanacheEntityBase {
    public Long id;
    public String name;

    public EntityShortModel(EntityBaseModel entityModel) {
        this.id = entityModel.id;
        this.name = entityModel.titre;
    }
}
