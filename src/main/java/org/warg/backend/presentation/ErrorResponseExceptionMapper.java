package org.warg.backend.presentation;

import java.net.MalformedURLException;

import java.io.IOException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.reactive.common.util.DateUtil.DateParseException;

@Provider
public class ErrorResponseExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    public Response toResponse(Throwable ex) {
        ex.printStackTrace();
        if (ex instanceof MalformedURLException) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid URL").build();
        } else if (ex instanceof IllegalArgumentException) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid argument").build();
        } else if (ex instanceof NumberFormatException) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid number format").build();
        } else if (ex instanceof DateParseException) {
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid date format").build();
        } else if (ex instanceof IOException) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("IOException").build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Internal server error").build();
        }
    }
}

