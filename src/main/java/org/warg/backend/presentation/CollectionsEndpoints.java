package org.warg.backend.presentation;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponseSchema;
import org.warg.backend.data.model.CollectionModel;
import org.warg.backend.domain.Core;
import org.warg.backend.presentation.model.CollectionPresentationModel;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/collections")
public class CollectionsEndpoints {

    @Inject
    Core core;

    @GET
    @Path("")
    @Operation(description = "Returns a simple string to test the API")
    @Produces(MediaType.APPLICATION_JSON)
    public List<CollectionModel> getCollections() {
        return core.getCollections();
    }

    @GET
    @Path("/names")
    @Operation(description = "Returns a simple string to test the API")
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getCollectionsNames() {
        return core.getCollectionsNames();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(CollectionModel.class)
    public CollectionModel getCollection(@PathParam("id") Long id) {
        return core.getCollection(id);
    }

    @PUT
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(CollectionModel.class)
    public CollectionModel createCollection(@PathParam("name") String name) {
        return core.createCollection(name);
    }

    @PATCH
    @Path("/{id}/rename/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(CollectionModel.class)
    public CollectionModel renameCollection(@PathParam("id") Long id, @PathParam("name") String name) {
        return core.renameCollection(id, name);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteCollection(@PathParam("id") Long id) {
        if (core.deleteCollection(id)) {
            return Response.ok().build();
        }
        else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @GET
    @Path("/{id}/entities")
    @Produces(MediaType.APPLICATION_JSON)
    public CollectionPresentationModel getCollectionEntities(@PathParam("id") Long id) {
        return core.getCollectionPresentation(id);
    }

    @PUT
    @Path("/{id}/add/{idE}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(CollectionModel.class)
    public CollectionModel addToCollection(@PathParam("id") Long id, @PathParam("idE") Long idE) {
        return core.addToCollection(id, idE);
    }

    @PUT
    @Path("/{id}/createadd/{idE}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(CollectionModel.class)
    public CollectionModel createAddToCollection(@PathParam("id") Long id, @PathParam("idE") String name) {
        return core.createAddToCollection(id, name);
    }

    @DELETE
    @Path("/{id}/entities/{idE}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(CollectionModel.class)
    public CollectionModel removeFromCollection(@PathParam("id") Long id, @PathParam("idE") Long idE) {
        return core.removeFromCollection(id, idE);
    }
}
