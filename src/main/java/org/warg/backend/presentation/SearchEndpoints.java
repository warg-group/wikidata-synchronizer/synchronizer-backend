package org.warg.backend.presentation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.warg.backend.data.model.EntitySearchModel;
import org.warg.backend.data.model.EntityStorable;
import org.warg.backend.domain.Core;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Path("/search")
public class SearchEndpoints {

    @Inject
    Core core;

    @Inject
    ObjectMapper mapper;

    @GET
    @Path("/{id}/{page}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSearch(
            @PathParam("id") Long id,
            @PathParam("page") Integer page) {
        List<EntityStorable> list = core.getSearch(id, page);
        if (list == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .entity(new HashMap<String, String>() {{put("error", "Invalid Index");}})
                    .build();
        }
        return Response.ok(list).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public EntitySearchModel getSearchModel(@PathParam("id") Long id) {
        return core.getSearchModel(id);
    }

    @PUT
    @Path("/{keyword}")
    @Produces(MediaType.APPLICATION_JSON)
    public EntitySearchModel createSearch(@PathParam("keyword") String keyword) {
        return core.createSearch(keyword);
    }

    @PUT
    @Path("/{id}/validate/{idc}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Long> validateSearch(@PathParam("id") Long id, @PathParam("idc") Long idc, String data) {
        List<Long> myObjects = new ArrayList<>();
        try {
            myObjects = mapper.readValue(data, new TypeReference<List<Long>>(){});
            for (Long s : myObjects) {
                System.out.println(s);
            }
        } catch (JsonProcessingException e) {
//            return Collections.singletonList("{\"error\": \"Invalid Index\"}");
        }
        return core.validateSearch(id, idc, myObjects);
    }
}
