package org.warg.backend.presentation;

import org.eclipse.microprofile.openapi.annotations.responses.APIResponseSchema;
import org.warg.backend.data.model.EntityModel;
import org.warg.backend.domain.Core;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/entities")
public class EntitiesEndpoints {

    @Inject
    Core core;

    @PATCH
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(EntityModel.class)
    public EntityModel refreshEntity(@PathParam("id") Long id) {
        return core.refreshEntity(id);
    }

    @DELETE
    @Path("/{id}")
    public Response deleteEntity(@PathParam("id") Long id) {
        if (core.deleteEntity(id)) {
            return Response.ok().build();
        }
        else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @PATCH
    @Path("/{id}/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(EntityModel.class)
    public EntityModel renameEntity(@PathParam("id") Long id, @PathParam("name") String name) {
        return core.renameEntity(id, name);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<EntityModel> getEntities() {
        return core.Entities();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(EntityModel.class)
    public EntityModel getEntity(@PathParam("id") Long id) {
        return core.getEntity(id);
    }

    @PUT
    @Path("/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    @APIResponseSchema(EntityModel.class)
    public EntityModel createEntity(@PathParam("name") String name) {
        return core.createEntity(name);
    }
}
