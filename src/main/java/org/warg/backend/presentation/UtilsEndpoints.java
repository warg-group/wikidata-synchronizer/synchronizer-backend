package org.warg.backend.presentation;

import io.quarkus.runtime.Quarkus;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.warg.backend.domain.Core;
import org.warg.backend.data.model.TemplateHashmap;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/utils")
public class UtilsEndpoints {

    @Inject
    Core core;

    @GET
    @Path("/hello")
    @Operation(description = "Returns a simple string to test the API")
    @Produces(MediaType.TEXT_PLAIN)
    public String testConnection() {
        return "Hello Test";
    }

    @GET
    @Path("/shutdown")
    @Operation(description = "Shutdown the application")
    public Response shutdown() {
        Quarkus.asyncExit();
        return Response.ok().build();
    }

    @GET
    @Path("/error/{code}")
    @Operation(description = "Returns an error with the given code")
    public Response error(@PathParam("code") int code) {
        return Response.status(code).entity("coucou c'est moi tchoupi").build();
    }

    @GET
    @Path("/templatehashmap")
    @Operation(description = "Returns a TemplateHashmap")
    @Produces(MediaType.APPLICATION_JSON)
    public TemplateHashmap getTemplateHashmap() {
        return new TemplateHashmap("Hello", 3.14f, null);
    }
}
