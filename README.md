# WARG

## Documentation

This project explains its endpoints [at this auto-generated page](https://warg-group.gitlab.io/wikidata-synchronizer/synchronizer-backend/).

## Running the application

You can run the application using Docker:
```shell script
docker pull registry.gitlab.com/warg-group/wikidata-synchronizer/synchronizer-backend
```


You can run your application in dev mode that enables live coding using:
```shell script
./gradlew quarkusDev
```

## Dependencies

This project uses mainly [Quarkus](https://quarkus.io/), a Java framework made of many others, mainly RESTeasy and Hibernate.

All current dependencies are reported [at this page](https://gitlab.com/warg-group/wikidata-synchronizer/synchronizer-backend/-/dependencies).

## Licenses

This project is licensed under the MIT License, available  [here](LICENSE). 

As for the dependencies, their licenses are listed [at this page](https://gitlab.com/warg-group/wikidata-synchronizer/synchronizer-backend/-/licenses#licenses).

